var express = require('express');
const { route } = require('..');
var router = express.Router();
var usuariosController = require('../../controlers/api/usuarioControllerApi'); 

router.get('/', usuariosController.usuarios_list);
router.post('/create', usuariosController.usuario_create);
router.post('/reservar', usuariosController.usuario_reservar);

module.exports = router;
