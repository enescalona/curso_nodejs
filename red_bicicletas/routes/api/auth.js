var express = require('express');
var router = express.Router();

const authController = require('../../controlers/api/authControllerApi');

router.post('/autenticate', authController.authenticate);
router.post('/forgotPassword', authController.forgotPassword);

module.exports = router;