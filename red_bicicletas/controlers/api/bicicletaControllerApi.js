var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.find({}, function(err, bicicleta){
        res.status(200).json({
            bicicleta: bicicleta
        });
    });
};

exports.bicicleta_create = function(req, res){

    var bici = new Bicicleta({id: req.body.id, color: req.body.color, modelo: req.body.modelo});
    bici.ubicacion = [req.body.lat, req.body.lng];
    bici.save(bici, function(err){
        res.status(200).json(bici);
    });
};

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(req.body.id, function(err){
        res.status(204).send();
    });
};