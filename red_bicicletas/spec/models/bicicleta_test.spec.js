var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', () => {
    beforeEach( (done) => {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', () => {
            console.log('Conectado al test Base datos');
            done();
        });
    });

    afterEach( (done) => {
        Bicicleta.deleteMany( {}, (err, succes) => {
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicics){
                expect(bicis.length).toBe(0);
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agregar solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "uebana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toBe.toEqual(aBici.code);
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "uebana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);
                    var aBici = new Bicicleta({code: 2, color: "rojo", modelo: "uebana"});
                    Bicicleta.add(aBici, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });


    describe('Bicicleta.removeByCode', () => {
        it('eliminar la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "uebana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);
                    var aBici = new Bicicleta({code: 2, color: "rojo", modelo: "uebana"});
                    Bicicleta.add(aBici, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.deleteByCode(1, function(err, targetBici){
                            if(err) console.log(err);
                            Bicicleta.allBicis(function(err, bicis){
                                expect(bicis.length).toEqual(1);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });


});

/*beforeEach(()=>{ Bicicleta.allBicis = [] });

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(2, 'blanca', 'urbana', [-34.596932, -58.3808287]);
        Bicicleta.add(a);    

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.finById', () => {
    it('debe devolver la bici con id 1', () => {

        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'blanca', 'urbana');
        var aBici2 = new Bicicleta(2, 'verde', 'urbana');
        Bicicleta.add(aBici); 
        Bicicleta.add(aBici2); 
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});

describe('Bicicleta.removeById', () => {
    it('debe eliminar la bici con id 1', () => {

        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'blanca', 'urbana');
        var aBici2 = new Bicicleta(2, 'verde', 'urbana');
        Bicicleta.add(aBici); 
        Bicicleta.add(aBici2); 
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1);
    });
});*/