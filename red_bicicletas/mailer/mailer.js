var nodemailer = require("nodemailer");
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig = {
    if(process.env.NODE_ENV === 'production'){
        const options = {
            auth: {
                api_key: process.env.API_KEY
            }
        }

        mailConfig = sgTransport(options);
    } else {
        if(process.env.NODE_ENV === 'staging'){
            const options = {
                auth: {}
                api_key: process.env.API_KEY
            }
        }
    }
};

let mailConfig = {

    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'brielle.farrell10@ethereal.email',
        pass: 'QtMWHKutJmaAv5KQHa'
    }
};

module.exports = nodemailer.createTransport(mailConfig);
