var mymap = L.map('main_map').setView([-34.769604,-56.379443], 9.28);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.mapbox-streets-v8',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'YOUR_MAPBOX_ACCESS_TOKEN'
}).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    succes: function(result){
        console.log();
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        });
    }
})